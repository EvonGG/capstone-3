import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner({data}) {

	const { title, content, destination, label } = data;

	return (
			<Row>
				<Col className="text-center text-dark">
					<h1>{title}</h1>
					<p>{content}</p>
					<Button as={Link} to={destination} variant="primary">{label}</Button>
				</Col>
			</Row>
	)
}