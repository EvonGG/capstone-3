import { useContext } from 'react'
import {  Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

  const { user } = useContext(UserContext);

  // State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));
  // console.log(user);

	return(
      <Navbar bg="danger" expand="lg">
        <Container className="font-weight-bold">
          <Navbar.Brand as={Link} to="/" className="text-dark">LechonArn Specials</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav float-right">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/" className="text-dark">Home</Nav.Link>

              {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/admin" end className="text-dark">Admin Dashboard</Nav.Link>
                :
                <Nav.Link as={ NavLink } to="/products" end className="text-dark">Products</Nav.Link>
              }

              {/*Conditional rendering such that the Logout link will be shown instead of the Login and Register when a user is logged in*/}
              {(user.id !== null)

                ?
                <Nav.Link as={NavLink} to="/logout" className="text-dark">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={NavLink} to="/login" className="text-dark">Login</Nav.Link>
                  <Nav.Link as={NavLink} to="/register" className="text-dark">Register</Nav.Link>
                </>
              }

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
	)
}