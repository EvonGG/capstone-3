import { Row, Col, Carousel } from 'react-bootstrap';
import lechon1 from '../photos/lechon1.jfif';
import mickey from '../photos/mickey.jpg';
import chick from '../photos/chick.jpg';
import '../App.css';

export default function Highlights(){
	return (
	<Row className="carousel-row mt-3 mb-3">
		<Col className="m-auto">	
			<Carousel className="carousel-tab flex p-4 img-fluid">
				<Carousel.Item interval={800} >
				<img className="rounded mx-auto d-block"
				src={lechon1}
				width={900} height={500}
				alt="First slide"
				/>
					<Carousel.Caption className="text-warning bg-dark font-bold" >
					<h3>The Best Lechon House in Ormoc</h3>
					<p>Awarded by the Ormoc Local Government Unit for its best tasting products.</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item interval={800}>
				<img className="rounded mx-auto d-block"
				src={mickey} width={900} height={500}
				alt="Second slide"
				/>
						<Carousel.Caption className="text-warning bg-dark font-bold">
						<h3>Delicious Organic Recipes</h3>
						<p>All products we sell are 100% organic.</p>
						</Carousel.Caption>
				</Carousel.Item >
				<Carousel.Item interval={800}>
				<img className="rounded mx-auto d-block"
				src={chick} width={900} height={500}
				alt="Third slide"
				/>
					<Carousel.Caption className="text-warning bg-dark font-bold">
					<h3>Freshly Served</h3>
					<p>
					We guarantee that our products are served/packed fresh to maintain its quality.
					</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>

		</Col>
	</Row>
	)
}